From Coq Require Import List.
From Proofs Require Import GHC.List.
From Src Require Import Queue.

Import ListNotations.

Definition models {a} (xs : list a) (q : Queue a) : Prop :=
  front q ++ rev (back q) = xs.

Lemma models_empty {a} : models [] (empty (a := a)).
Proof.
  reflexivity.
Qed.

Lemma models_push {a} : forall (x : a) (xs : list a) (q : Queue a),
    models xs q -> models (xs ++ [x]) (push x q).
Proof.
  intros x xs q Eq.
  unfold models in *.
  destruct q as [front_q back_q].
  cbn in *.
  rewrite app_assoc.
  rewrite Eq.
  reflexivity.
Qed.

Lemma models_pop {a} : forall (xs : list a) (y : a) (q : Queue a),
    models (y :: xs) q ->
    exists q',
      pop q = Some (y, q') /\
      models xs q'.
Proof.
  intros xs y q Eq.
  unfold models in *.
  destruct q as [front_q back_q].
  cbn in *.
  destruct front_q as [ | x front_q' ].
  - (* front is empty *)
    cbn in Eq.
    rewrite GHC.List.hs_coq_reverse.
    rewrite Eq.
    exists (MkQueue xs []).
    split.
    + reflexivity.
    + cbn.
      rewrite app_nil_r.
      reflexivity.
  - (* front is not empty *)
    cbn in Eq.
    exists (MkQueue front_q' back_q).
    injection Eq.
    intros Eq' Ey.
    split.
    + rewrite Ey. reflexivity.
    + cbn. apply Eq'.
Qed.

(* Examples *)

Example example1 : models [1; 2; 3] (push 3 (push 2 (push 1 empty))).
Proof.
  pose proof (models_empty (a := nat)) as H.
  apply (models_push 1) in H. cbn [ app ] in H.
  apply (models_push 2) in H. cbn [ app ] in H.
  apply (models_push 3) in H. cbn [ app ] in H.
  apply H.
Qed.

Infix "<$>" := option_map (at level 40).

Example example2 :
  fst <$> pop (push 3 (push 2 (push 1 empty))) = Some 1.
Proof.
  pose proof example1 as H.
  apply models_pop in H.
  destruct H as [q' [Hpop Hq']].
  rewrite Hpop.
  reflexivity.
Qed.

Example example3 {a} : forall (q : Queue a) (x y : a) (xs : list a),
  models (x :: xs) q ->
  fst <$> pop (push y q) = fst <$> pop q.
Proof.
  intros q x y xs Hq.
  pose proof (models_push y (x :: xs) q Hq) as Hqy.
  cbn in Hqy.
  apply models_pop in Hq.
  apply models_pop in Hqy.
  destruct Hq as [q' [Eq' Hq']].
  destruct Hqy as [qy' [Eqy' Hqy']].
  rewrite Eq', Eqy'.
  cbn.
  reflexivity.
Qed.
