module Queue where

data Queue a = Queue { front :: [a], back :: [a] }

empty :: Queue a
empty = Queue [] []

push :: a -> Queue a -> Queue a
push x (Queue front back) = Queue front (x : back)

pop :: Queue a -> Maybe (a, Queue a)
pop (Queue (y : front) back) = Just (y, Queue front back)
pop (Queue [] back)
  = case reverse back of
      [] -> Nothing
      y : front -> Just (y, Queue front [])

toList :: Queue a -> [a]
toList q
  = case pop q of
      Nothing -> []
      Just (y, q') -> y : toList q'
